var info = {
				last: 0,
				buy: 0,
				sell: 0,
				volume: 0
			}
			var price = {
				USD:0,
				GBP:0,
				EUR:0
			}
			var updateInterval = 5000 //5 seconds
			Vue.mixin({
				data(){
					return {
						price: price
					}
				},
				methods:{
					loadPrice: function(){
						console.log('Load Price');
						axios
							.get('https://api.coindesk.com/v1/bpi/currentprice.json')
							.then(response=>(this.price = response.data.bpi))
					}
				},
				filters: {
					currency (value) {
						return parseFloat(value).toFixed(2)
					},
					satoshi (value) {
						return parseFloat(value).toFixed(8)
					}
				},
				mounted(){
					this.loadPrice()
					setInterval(function(){
						this.loadPrice()
					}.bind(this),updateInterval)
				}
			})
			new Vue({
				el: '#absolute',
				data(){
					return{
						info: info,
						asset: 1000
					}
				},
				methods: {
					loadData: function(){
						console.log('Load data Absolute');
						axios
							.get('https://graviex.net/api/v2/tickers/absbtc.json')
							.then(response=>(this.info = response.data.ticker))
					}
				},
				mounted(){
					this.loadData()
					setInterval(function(){
						this.loadData()
					}.bind(this),updateInterval)
				}
			})
			new Vue({
				el: '#dextro',
				data(){
					return{
						info: info,
						asset: 1000
					}
				},
				methods: {
					loadData: function(){
						console.log('Load data Dextro');
						axios
							.get('https://graviex.net/api/v2/tickers/dxobtc.json')
							.then(response=>(this.info = response.data.ticker))
					}
				},
				mounted(){
					this.loadData()
					setInterval(function(){
						this.loadData()
					}.bind(this),updateInterval)
				}
			})
			new Vue({
				el: '#z01',
				data(){
					return{
						info: info,
						asset: 1000
					}
				},
				methods: {
					loadData: function(){
						console.log('Load data Dextro');
						axios
							.get('https://graviex.net/api/v2/tickers/zocbtc.json')
							.then(response=>(this.info = response.data.ticker))
					}
				},
				mounted(){
					this.loadData()
					setInterval(function(){
						this.loadData()
					}.bind(this),updateInterval)
				}
			})
			new Vue({
				el: '#ipsum',
				data(){
					return{
						info: info,
						asset: 1000
					}
				},
				methods: {
					loadData: function(){
						console.log('Load data Dextro');
						axios
							.get('https://graviex.net/api/v2/tickers/ipsbtc.json')
							.then(response=>(this.info = response.data.ticker))
					}
				},
				mounted(){
					this.loadData()
					setInterval(function(){
						this.loadData()
					}.bind(this),updateInterval)
				}
			})